<?php 
    require_once 'functions.php';
?>

<?php if(isLogged()): ?>
    <div style="text-align: right;">
        <a href="/logout.php">Se deconnecter</a>
    </div>
<?php else : ?>
    <div style="text-align: right;">
        <a href="/login.php">Se connecter</a>
    </div>
<?php endif; ?>