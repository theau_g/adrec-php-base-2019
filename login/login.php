<?php 
    require_once 'functions.php';
    session_start();

    if (!empty($_POST)) {
        if (!empty($_POST['login']) && !empty($_POST['password'])) {
            logIn($_POST['login'], $_POST['password']);
        }
    }

?>

<style>
    label {
        display: block;
        margin-top: 0.5rem;
    }
</style>

<div style="text-align: center;">
    <h1>
        Connexion
    </h1>

    darksasuke43
    <form method="post">
        <div>
            <label for="login">
                Login
            </label>
            <input id='login' type="text" name="login" required>

        </div>
        <div>
            <label for="password">
                Mot de passe
            </label>
            <input id="password" type="password" name="password" required>
        </div>

        <?php if ($_GET && $_GET['auth'] == 'fail'): ?>
            <div style="margin: 1.5rem 0; color: red;">
                 Login ou mot de passe incorrect
            </div>
        <?php endif; ?>


        <div>
            <button type="submit">Envoyer</button>
        </div>
    </form>
</div>