<?php
session_start();
require_once 'functions.php';



/*
    Systeme de login/mot de passe
    Au niveau de la session -> loggedIn = true

    Fichiers :
    - function.php -> $tableau avec le login mot de passe
        -> toutes les fonctions de notre site
        -> Fonction qui permet de selection un user dans le tableau (via son login)
    - logout.php -> permet de me deconnecter
    - index.php -> page d'acceuil
    - liste-des-membre.php -> Affiche la liste des utilisateur -> redirect si non log
    - login.php -> Gère le formulaire de login et l'indentification
*/

include 'partials/navigation.php';
?>



<h1 style="text-align: center;">
    Salutation
</h1>

<p>
Lorem, ipsum dolor sit amet consectetur adipisicing elit. Expedita delectus provident voluptates fuga ipsum doloribus repellendus minus omnis ad illo cumque, assumenda explicabo, iure enim reiciendis eum eius obcaecati repudiandae. Voluptatibus, earum quaerat expedita tempora quo cupiditate! Velit tempora aliquam aperiam temporibus quasi assumenda ipsa et odio at, nulla hic quisquam sed officiis dolores saepe reiciendis molestiae non tempore. Adipisci nulla inventore sint repellendus iusto quam et tempore! Est expedita ab illum libero placeat consequuntur asperiores cumque ducimus fugiat rem dolorum, molestiae mollitia aspernatur, vel alias labore quo error officia ad. Ducimus officiis a sed? Repellendus est delectus quos ratione beatae quod eaque, aliquid unde ullam nam minima quae! Quos, vero facere incidunt at provident laudantium cumque illo adipisci porro, voluptatum quidem magnam? Harum neque sapiente animi doloribus. Atque, iusto distinctio molestias tenetur recusandae odio accusamus laudantium iste omnis reiciendis, eum modi quibusdam eaque tempora exercitationem aliquam ad culpa veritatis enim facere officiis. Harum similique qui quia ex obcaecati voluptate necessitatibus eaque officiis. Deleniti non facere sit exercitationem ut veniam voluptatem, rerum quis ducimus debitis? Similique, beatae, ducimus at amet voluptate magnam nostrum perferendis sapiente minus distinctio et a repellendus doloribus! Necessitatibus excepturi saepe aut sapiente cumque voluptatum consequatur animi corporis eligendi, quos non odio molestiae doloribus ullam deleniti quidem voluptatibus. Dolore perferendis nobis provident, quisquam, natus omnis, facere facilis ipsum dolor impedit asperiores molestias dolorem culpa hic ab. Esse dignissimos vero deleniti exercitationem incidunt expedita vitae corrupti adipisci, aliquid reiciendis et, error nam facere totam excepturi. Voluptatem, nobis magni?
</p>