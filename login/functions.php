<?php

//Simulation de la base

//Pour encoder les mots de passe -> password_hash() puis copier le hash le tableau

const USERS = [
    [
        'login' => 'theau_g',
        //thebossdu63
        'password' => '$2y$10$yuwHrDESWIqCIV.LvxUs5OI9xyPFHu2meYrvkaIIFx51JaTS.fEvm',
    ],
    [
        'login' => 'tho_mas',
        //darksasuke43
        'password' => '$2y$10$sMCtWb15Cx/Av0Bg66lHtuxoT.XGLRDFGI6d8W.iv37Hkl9RnN0..',
    ],
];

function redirectIsLogged()
{
    session_start();

    if (!isset($_SESSION['loggedIn'])) {
        redirect('/login.php');
    }

}

function isLogged()
{
    if(isset($_SESSION['loggedIn'])) {
        return true;
    } else {
        return false;
    }
}

function redirect(string $path): void
{
    header('Location: ' . $path);
}

function logIn(string $login, string $password): void
{
    //Récuperer le tableau de l'utilisateur qui correspond au login
    //Utiliser password_verify pour controller si le mot de passe est valide
    //Ajouter la variable loggedIn à la session = true
    //Redirection vers la home

    $find = false;

    foreach (USERS as $user){
      if ($user['login'] == $login) {
        if (password_verify($password, $user['password'])) {
            $_SESSION['loggedIn'] = 'sasuke';
            $find = true;
            break;
        }
      } 
    }

    if($find){
        redirect('/index.php');
    } else {
        redirect('/login.php?auth=fail');
    }
}