<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Les formulaires</title>
  </head>
  <body>
    

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <h1 class="mt-4">Les formulaires</h1>

                <form method="GET">
                    <label for="nameID">Prénom</label>
                    <input 
                    class="form-control"
                    type="text" 
                    id="nameID" 
                    placeholder="Ex: Pascal" 
                    name="first_name"
                    required
                    >

                    <input class="form-control mt-2" type="number" value="0" min="0" max="3">
                </form>
                
                <?php if($_GET && !empty($_GET['first_name'])): ?>
                    <h2>Bienvenue à <?php echo $_GET['first_name']; ?></h2>
                <?php endif; ?>

                <h2 class="text-center mt-4">
                    Générateur de mentions légales
                </h2>

                <form class="mt-4" action="private-policy-generator.php" method="post">
                    <!-- Mail -->
                    <input type="email" class="form-control mt-2" name="email" placeholder="Votre email">
                    <!-- Nom prenom -->
                    <input type="text" class="form-control mt-2" name="name" placeholder="Votre nom">
                    <!-- Nom d'entreprise -->
                    <input type="text" class="form-control mt-2" name="society_name" placeholder="Nom de votre entreprise">
                    <!-- Numéro de siret -->
                    <input type="text" class="form-control mt-2" name="siret" placeholder="Numéro de SIRET">
                    <!-- url du site -->
                    <input type="url" class="form-control mt-2" name="url" placeholder="Url du site de la société">

                    <div class="text-center mt-2">
                        <button type="submit" class="btn btn-primary">Envoyer</button>
                    </div>
                </form>        
            </div>
        </div>
    </div>

    














    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>