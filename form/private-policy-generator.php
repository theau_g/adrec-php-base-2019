<?php

/**
 * Genérer une page HTML qui simule une page légale avec les données du formulaire.
 * La page contient des sections avec des titres, des liens, des mots en gras.
 * 
 * - pour l'url ne pas afficher le https devant (use parse_url)
 * - mail -> use mailto:
 * - Le reste des variable (nom, siret, ...) doit etre en gras.
 */
//var_dump($_POST);

if ($_POST && !empty($_POST)) {

    $emptyFeild = false;

    if (!empty($_POST['url'])) {
        $url = $_POST['url'];
    } else {
        echo "Entrez l'url <br>";
        $emptyFeild = true;
    }

    if (!empty($_POST['email'])) {
        $mail = $_POST['email'];
    } else {
        echo "Entrez le mail <br>";
        $emptyFeild = true;
    }

    if (!empty($_POST['society_name'])) {
        $society_name = $_POST['society_name'];
    } else {
        echo "Entrez le society_name <br>";
        $emptyFeild = true;
    }

    if (!empty($_POST['name'])) {
        $name = $_POST['name'];
    } else {
        echo "Entrez le name <br>";
        $emptyFeild = true;
    }

    if (!empty($_POST['siret'])) {
        $siret = $_POST['siret'];
    } else {
        echo "Entrez le siret <br>";
        $emptyFeild = true;
    }

    //Il faut verifier tout les champs un par un (ou faire un boucle)

    //Après le test de chaque variable
    if ($emptyFeild == true) {
        die();
    }

    function displayUrl($url)
    {
        $urlParsed = parse_url($url);
        echo '<a target="_blank" href="' . $url . '">' . $urlParsed['host'] . '</a>';
    }

    function displayMail($mail)
    {
        echo '<a href="mailto:' . $mail . '">' . $mail . '</a>';
    }

    function dStrong($var)
    {
        echo "<strong>$var</strong>";
    }
} else {
    die("Aucune valeur dans la tableau");
}


?>

<h2>Informations légales</h2>
<h3>1. Présentation du site.</h3>
<p>En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique, il est précisé aux utilisateurs du site <?php displayUrl($url); ?> l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi :</p>
<p>
    <strong>Propriétaire</strong> : <?php dStrong($society_name) ?> - <?php dStrong($siret) ?> - 3 rue des Parapluies - 63000 Clermont-Ferrand<br />
    <strong>Créateur</strong> :<?php dStrong($society_name) ?> - <?php displayUrl($url) ?><br />
    <strong>Responsable publication</strong> : <?php dStrong($name) ?> - <?php displayMail($mail) ?><br />
    Le responsable publication est une personne physique ou une personne morale.<br />
    <strong>Webmaster</strong> : <?php dStrong($name) ?> - <?php displayMail($mail) ?><br />
    <strong>Hébergeur</strong> : <?php dStrong($society_name) ?> - 58 Allée du Pont de la Sarre 63000 Clermont Ferrand;
</p>
<h3>2. Conditions générales d’utilisation du site et des services proposés.</h3>
<p>L’utilisation du site <?php displayUrl($url); ?> implique l’acceptation pleine et entière des conditions générales d’utilisation ci-après décrites. Ces conditions d’utilisation sont susceptibles d’être modifiées ou complétées à tout moment, les utilisateurs du site <?php displayUrl($url); ?> sont donc invités à les consulter de manière régulière.</p>
<p>Ce site est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par PROPRIETAIRE, qui s’efforcera alors de communiquer préalablement aux utilisateurs les dates et heures de l’intervention.</p>
<p>Le site <?php displayUrl($url); ?> est mis à jour régulièrement par Adrien Bel. De la même façon, les mentions légales peuvent être modifiées à tout moment : elles s’imposent néanmoins à l’utilisateur qui est invité à s’y référer le plus souvent possible afin d’en prendre connaissance.</p>
<h3>3. Description des services fournis.</h3>
<p>Le site <?php displayUrl($url); ?> a pour objet de fournir une information concernant l’ensemble des activités de la société.</p>