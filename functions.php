<?php

function sayHello(string $firstName): string 
{
    return "Bonjour $firstName <br>";
}

function sayGoodBye(): void
{
    echo "Good bye";
}

sayGoodBye();
echo sayHello('Theau');

/**
 * Retourne la plus petite valeur du tableau
 * @param array $tab
 * @return float|int
 */
function minVal(array $tab): float
{
    $min = $tab[0];

    foreach ($tab as $value) {
        if ($value < $min) {
            $min = $value;
        }  
    }

    return $min;
}

$ages =[21, 18, 10, 5, 33, 7, 55];

echo minVal($ages);

