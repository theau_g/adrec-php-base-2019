<?php
//Test si name dans la session -> si pas de nom -> redirect vers set-name.php
// delete-name.php -> se charge juste de supprimer le nom de la session + redirect

//Pour moi temp = 	C:\Users\user\AppData\Local\Temp

//header('Location: contact.php');

/*Un formulaire qui vous demande votre nom à la premiere connexion, et qui 
affiche ensuite notre nom en haut de la page
- Faire ce formulaire sur une page à part et donner la possibilité à l'utilisateur
de changer de nom
- Redirect header('Location: index.php');
- Ajouter : si plus de prenom, on redirige vers la page de formulaire
- Ajouter boutton pour supprimer son prénom

Les pages : 
 - index.php -> page d'accueil du site -> Ici je met le "Bonjour Machin"
 - set-name.php -> Page du formulaire de choix du prénom -> redirection vers l'index

Les outils:
 - $_SESSION[]
 - session_start()
 - session_unset()
 - session_destroy()
 - header('Location: votre_url');
 */

session_start();

if (empty($_SESSION['name'])){
    header('Location: set-name.php');
}
?>

<div style="text-align: right;">
    <a href="/delete-name.php">Effacer mon nom</a>
</div>

<h1 style="text-align: center;">
    Bonjour <?php echo $_SESSION['name']; ?>
</h1>

<p>
Lorem, ipsum dolor sit amet consectetur adipisicing elit. Expedita delectus provident voluptates fuga ipsum doloribus repellendus minus omnis ad illo cumque, assumenda explicabo, iure enim reiciendis eum eius obcaecati repudiandae. Voluptatibus, earum quaerat expedita tempora quo cupiditate! Velit tempora aliquam aperiam temporibus quasi assumenda ipsa et odio at, nulla hic quisquam sed officiis dolores saepe reiciendis molestiae non tempore. Adipisci nulla inventore sint repellendus iusto quam et tempore! Est expedita ab illum libero placeat consequuntur asperiores cumque ducimus fugiat rem dolorum, molestiae mollitia aspernatur, vel alias labore quo error officia ad. Ducimus officiis a sed? Repellendus est delectus quos ratione beatae quod eaque, aliquid unde ullam nam minima quae! Quos, vero facere incidunt at provident laudantium cumque illo adipisci porro, voluptatum quidem magnam? Harum neque sapiente animi doloribus. Atque, iusto distinctio molestias tenetur recusandae odio accusamus laudantium iste omnis reiciendis, eum modi quibusdam eaque tempora exercitationem aliquam ad culpa veritatis enim facere officiis. Harum similique qui quia ex obcaecati voluptate necessitatibus eaque officiis. Deleniti non facere sit exercitationem ut veniam voluptatem, rerum quis ducimus debitis? Similique, beatae, ducimus at amet voluptate magnam nostrum perferendis sapiente minus distinctio et a repellendus doloribus! Necessitatibus excepturi saepe aut sapiente cumque voluptatum consequatur animi corporis eligendi, quos non odio molestiae doloribus ullam deleniti quidem voluptatibus. Dolore perferendis nobis provident, quisquam, natus omnis, facere facilis ipsum dolor impedit asperiores molestias dolorem culpa hic ab. Esse dignissimos vero deleniti exercitationem incidunt expedita vitae corrupti adipisci, aliquid reiciendis et, error nam facere totam excepturi. Voluptatem, nobis magni?
</p>