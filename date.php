<?php 

function dump($var)
{
    echo "<pre>" . var_export($var, true) . "</pre>";
}

date_default_timezone_set('Europe/Paris');
/*
echo date('d m Y h:i:s', 100);

var_dump(time());

echo '<br>';

echo date('d m Y', strtotime('+5 week'));


$nico = '26-01-1989';


var_dump(strtotime($bithAlexis));
var_dump(strtotime($nico));

if(strtotime($bithAlexis) > strtotime($nico)) {
    echo "<br> Alexis est plus jeune que nico";
}

*/

$birthAlexis = '18-03-1994';

$dateTimeAlexis = DateTime::createFromFormat('d-m-Y', $birthAlexis);


$date = new DateTime();


// dump($date);
// dump($dateTimeAlexis);

// echo $date->format('d/m/Y');
// echo '<br>';
// echo $date->format('h:i');


if($dateTimeAlexis > $date) {
    //echo 'Alexis vient du futur';

} else {
    //echo "Alexis est comme tout le monde";
}

$dateTimeAlexis->add(new DateInterval('P1Y10D'));

//dump($dateTimeAlexis);


$orderDate = DateTime::createFromFormat("d m Y H:i", '08 11 2019 13:40');
//dump($orderDate);

$testOrderDate = clone $orderDate;

//dump(new DateTime());

// if($testOrderDate->add(new DateInterval('PT10M')) > new DateTime()){
//     echo "Votre commande est expirée";
// } else {
//     echo "Tout va bien";
// }


// dump($orderDate);

/*

1 - Créer un objet DateTime qui contient votre date de naissance et l'afficher sous
les format suivant : 10/06/2019 10-06-2019 ou 10 06 2019


2 - Créer le 25 septembre 2019 avec le format suivant 25/09/2019

3 - Donner le jour (lundi mardi) du 14/07/1789

4 - Calculer votre age en fonction de votre de naissance

5 - Afficher la date des 10 derniers Dimanche

*/

//Exo 1
$birthDate = DateTime::createFromFormat('d m Y', '15 05 1984');
echo 'Exo 1 <br>';
echo $birthDate->format('d m Y') . ' <br>';
echo $birthDate->format('d-m-Y') . ' <br>';
echo $birthDate->format('d/m/Y') . ' <br>';

echo "==================================================== <br>";

//Exo 2
$exo2Date = DateTime::createFromFormat('d m Y', '25 09 2019');

echo 'Exo 2 <br>';
echo $exo2Date->format('d/m/Y') . ' <br>';
echo "==================================================== <br>";

//Exo 3 
$onEnAGros = DateTime::createFromFormat('Y-m-d', '1789-07-14');
echo 'Exo 3 <br>';
echo "Le " . $onEnAGros->format('d F Y') . " était un " . $onEnAGros->format('l') .'<br>';
echo "==================================================== <br>";

//Exo 4

$date1 = DateTime::createFromFormat('Y-m-d', '1996-08-26');
$currectDate = new DateTime();

echo 'Exo 3 <br>';
$interval = $date1->diff($currectDate);
echo $interval->format('%Y ans %m mois et %d jours') . '<br>';


echo "==================================================== <br>";

$lastSunday = new DateTime();

$lastSunday->setTimestamp(strtotime('last Sunday'));

for ($i = 0; $i < 10; $i++) {
    echo $lastSunday->format('d/m/Y') . '<br>';
    $lastSunday->sub(new DateInterval('P7D'));
}