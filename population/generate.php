<?php

$firstNames = ['Bob', 'Bill', 'Greg', 'Jack', 'John', 'Ryan', 'Jacob', 'Aaron', 'Juliette', 'Marine', 'Chloe', 'Emma', 'Laurent'];
$lastNames = ['Martin', 'Smith', 'Clinton', 'Doe', 'Marcus', 'Petiteau', 'Goncalves', 'Alcala', 'Amblard', 'Pauly', 'Gondek'];
$genders = ['male', 'female'];

$data = [];

for ($i=0; $i<1000; $i++) {
    $firstName = $firstNames[array_rand($firstNames)];
    $lastName = $lastNames[array_rand($lastNames)];
    $gender = $genders[array_rand($genders)];
    $married = mt_rand(0,1);
    $age = mt_rand(1, 100);
    $IQ = mt_rand(40, 150);


    if ($age < 18) {
        $married = 0;
    }

    $data[] = [
        'first_name' => $firstName,
        'last_name' => $lastName,
        'age' => $age,
        'gender' => $gender,
        'married' => $married,
        'IQ' => $IQ,
    ];
}

echo '<?php return ';
var_export($data);
echo ';';
