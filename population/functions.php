<?php

function dump($var)
{
    echo "<pre>" . var_export($var, true) . "</pre>";
}

/**
 * @param array $user
 * @param int $limit
 */
function listUsers(array $users, int $limit = 24): void
{

    $count = 1;

    foreach ($users as $user) {
       echo "<strong>Nom :</strong> " . $user['last_name'] . "<br>";
       echo "<strong>Prénom :</strong> " . $user['first_name'] . "<br>";
       echo "<strong>Age :</strong> " . $user['age'] . "<hr>";

       if($count >= $limit){
        break;
       }

       $count++;
       
    }
}

/**
 * Retourne le user le plus jeune
 * @param array $users
*/
function youngestUser(array $users): array
{
    $minAgeUser = $users[0];
    
    foreach ($users as $user) {
        if ($user['age'] < $minAgeUser['age']) {
            $minAgeUser = $user;
        }
    }

    return $minAgeUser;
}

/**
 * Retourne la personne qui a le QI le plus bas
 */
function lowestIQ(array $users)
{
    $minIQUser = $users[0];
    
    foreach ($users as $user) {
        if ($user['IQ'] < $minIQUser['IQ']) {
            $minIQUser = $user;
        }
    }

    return $minIQUser;
}

$tab = [
    'Theau' => 20,
    'Emma' => 21,
    'John' => 45,
];

$tab['Theau'] = 1;

/**
 * Retourne un tableau avec les prénoms et leur nombre (Du + au - courant)
 * @param array $users
 * @return array
 */
function nameCurrency(array $users): array
{
    $namesCount = [];

    foreach ($users as $user) {
        if (array_key_exists($user['first_name'], $namesCount)){
            $namesCount[$user['first_name']]++;
        } else {
            $namesCount[$user['first_name']] = 1;
        }
    }

    arsort($namesCount);

    return $namesCount;
}


function findPeopleByAge(array $users, int $age = 18): array
{
    $resultUsers = [];

    foreach ($users as $user) {
       if ($user['age'] == $age) {
            $resultUsers[] = $user;
       }
    }

    return $resultUsers;
}

/**
 * @param array $users
 */
function avgIQ(array $users): float
{
    $total = 0;

    foreach ($users as $user) {
       $total += $user['IQ'];
    }

    return round(($total / count($users))  , 2);
}

function marriedPeople(array $users, int $min, int $max, string $gender): float
{
    if($min < 16) {
        die("I call the police");
    }

    $count = 0;

    foreach ($users as $user) {
        $age = $user['age'];

        if($age >= $min && $age <= $max && $user['married'] == 1){
            if($gender == 'all'){
                $count++;
            } elseif ($gender == $user['gender']){
                $count++;
            }
        }
    }
    $percentage = ($count / count($users)) * 100;
    return round($percentage, 2);
}