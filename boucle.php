<?php


/*
* Boucle for
*/


$lessons = [
    'PHP',
    'JS',
    'Droit',
    'Photoshop',
    'GIT',
    "Histoire de l'informatique",
    'Projet',
];

//Afficher les valeurs d'un tableau
for ($i = 0; $i < count($lessons); $i++) {
    //echo $lessons[$i] . '<br>';
}


//Equivalent de array_sum
$notes = [15, 12, 18, 8];
$sum = 0;
for ($i = 0; $i < count($notes); $i++) {
    $sum += $notes[$i];
}

//echo "Somme :  $sum";

$girls = [
    'Marine',
    'Emma',
];

$boys = [
    'Thomas',
    'Laurent',
    'Alain',
    'Nour',
];


//Equivalement à array_merge
//$guests = $girls;
// for ($i = 0; $i < count($boys) ; $i++) { 
//    $guests[] = $boys[$i];
// }

$guests = array_merge($boys, $girls);

//var_dump($guests);

/*
* Boucle while
*/

$ages = [23, 7, 4, 14, 21, 52, 19, 22, 8];

$i = 0;
$winner = 0;

while ($winner < 3 ) {
    if ($ages[$i] >= 18) {
        //echo $ages[$i] . '<br>';
        $winner++;
    } else {
        //echo "Trop jeune : " . $ages[$i] ."<br>";
    }

    $i++;
}

/*
* Boucle do while
*/

$tWater = 99;

do {
    //echo "Temperature de l'eau : $tWater <br>";
    $tWater--;
} while ($tWater <= 100 && $tWater >= 0);

/*
* Boucle foreach
*/


foreach ($guests as $firstName) {
    echo "$firstName <br>";
}


//$ages -> Donner moi la plus petite valeur du tableau

$ages =[21, 18, 10, 5, 33, 7, 55];

$min = $ages[0];

foreach ($ages as $age) {
    if ($age < $min) {
        $min = $age;
        
    }  
}

//echo "l'age le plus petit est $min";
