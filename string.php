<?php

function dump($var)
{
    echo "<pre>" . var_export($var, true) . "</pre>";
}


/**
 * https://www.php.net/manual/en/function.trim.php
 * Supprime les espaces (ou d'autres caractères) en début et fin de chaîne
 */
$hello = '      Hello, ça va ?';
//echo trim($hello);

/**
 * https://www.php.net/manual/fr/function.strlen.php
 * Calcule la taille d'une chaîne
 */

$password = "PercevalLeBoss1882456985452";
if (strlen($password) < 8 || strlen($password) > 16){
    //die("Votre mot de passe ne respecte pas le format (Entre 8 et 16 caractères)");
}


/**
 * https://www.php.net/manual/fr/function.str-replace.php
 */

$fileName = 'learn_php_with_theau_in_103.php';
//echo $fileName . '<br>';

$fileName = str_replace('_', '-', $fileName);
//echo $fileName;

$bannedWords = [
    'série',
    'légende',
    'saison'
];

$text = "Kaamelott est une série télévisée française humoristique et dramatique de fantasy historique créée par Alexandre Astier, Alain Kappauf et Jean-Yves Robin et diffusée entre le 3 janvier 2005 et le 31 octobre 2009 sur M6.

L'origine du nom de la série est bien évidemment la cité de Camelot, avec une orthographe et une graphie particulières faisant ressortir les initiales AA de l'auteur, et les deux T finaux forçant l'homophonie avec camelote.

De plus en plus populaire depuis 2006, la série s’inspire de la légende arthurienne et apporte une vision décalée de la légende en présentant un roi Arthur qui peine à être à la hauteur de la tâche que les dieux lui ont confiée. Entouré de chevaliers de la Table ronde passablement incompétents, confronté à la chute de l’Empire romain et aux incessantes incursions barbares, il doit encore trouver le Saint Graal.

Humoristique dans ses premières saisons, la série commence à prendre une tournure plus orientée comédie dramatique à partir de sa quatrième saison, avant de basculer plus significativement dans le dramatique lors de la suivante. Au cours de cette évolution, la série a étendu la durée de ses épisodes, passant d'un format shortcom à une durée plus longue, atteignant les trois quarts d'heure dans sa sixième et ultime saison.

L'évolution du format et des scénarios transforment ce qui était une série en feuilleton télévisé.

Le 22 janvier 2019, Alexandre Astier annonce le début du tournage1 de Kaamelott - Premier Volet pour une sortie prévue le 14 octobre 2020.";


//echo "<p>$text<p>";
//echo '<p>' . str_replace($bannedWords, '*******', $text) . '</p>';

//explode


//Les tags sont séparés par des virgules

$inputTags = "php, programmation, adrec, formation";

$inputTags = str_replace(' ', '', $inputTags);

//var_dump($inputTags);

$tags = explode(',', $inputTags);

//var_dump($tags);


//https://www.php.net/manual/fr/function.implode.php

$ingredients = [
    'salade',
    'tomate',
    'oignon',
];

//echo "Pour faire un bon kebab, il faut au moins : " . implode(', ', $ingredients);


$firstName = "Théau";

//echo substr($firstName, 0, 1);


$passwd = "Adrec122";
$hashedPasswd = md5($passwd);
//var_dump($hashedPasswd);

$dbPass = "194ad04383367ed1d0ef479870960556";

if (md5($passwd) == $dbPass) {
    echo "Good password";
}


$url = "https://www.youtube.com/watch?v=FySZp_dasNo&list=RDtbi8Rf_zBEI&index=2";
$url = "https://youtu.be/FySZp_dasNo?list=RDtbi8Rf_zBEI";
$params = [];
$parsedUrl = parse_url($url);

parse_str($parsedUrl['query'], $params);

dump($parsedUrl);